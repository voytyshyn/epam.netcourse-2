using System;

namespace Task1
{
	public class Program
	{
		public static void Main()
		{
			Vector vector1 = new Vector(10,20,30);
			Vector vector2 = new Vector(3,6,9);
			Vector vector3 = new Vector(1,1,1);
			Vector result;
			
			Console.WriteLine("1st vector {0} ", vector1);
			Console.WriteLine("2st vector {0} ", vector2);
			Console.WriteLine();
			
			result = vector1 + vector2;
			Console.WriteLine("Add 1st & 2nd vectors - {0}", result);
			
			result = vector1 - vector2;
			Console.WriteLine("Subtract 1st & 2nd vectors - {0} ", result);
		
			result = vector1 - vector2;
			Console.WriteLine("Product 1st & 2nd vectors - {0}", result);
			
			double res = Vector.scalar(vector1, vector2);
			Console.WriteLine("Scalar product 1st & 2nd vectors - {0}", res);
			
			res = Vector.TripleProduct(vector1, vector2, vector3);
			Console.WriteLine("Triple product 1st, 2nd, 3rd vectors - {0}", res);
			
			res = Vector.AngleBetween(vector1, vector2);
			Console.WriteLine("angle between two vectors 1st & 2nd vectors - {0}", res);
			
			Console.WriteLine("Length of first vector is greater {0}:", vector1 < vector2);
		}
	}

	public class Vector
	{
		#region Constructors
		public Vector(double x, double y, double z)
		{
			this.X = x;
			this.Y = y;
			this.Z = z;
		}
		#endregion
		
		#region Properties
		public double X { get; private set; }
		public double Y { get; private set; }
		public double Z { get; private set; }
		#endregion
		
		#region Methods
		//Vector addition
		public static Vector operator +(Vector fVector, Vector sVector)
		{
			return new Vector(fVector.X + sVector.X, fVector.Y + sVector.Y, fVector.Z + sVector.Z);
		}
		
		//Subtract vectors
		public static Vector operator -(Vector fVector, Vector sVector)
		{
			return new Vector(fVector.X - sVector.X, fVector.Y - sVector.Y, fVector.Z - sVector.Z);
		}
		
		//Vector product of two vectors
		public static Vector operator * (Vector fVector, Vector sVector)
		{
			return new Vector(fVector.X * sVector.X, fVector.Y * sVector.Y, fVector.Z * sVector.Z);
		}
		
		//The scalar product of two vectors
		public static double scalar(Vector fVector, Vector sVector)
		{
			return fVector.X * sVector.X + fVector.Y * sVector.Y + fVector.Z * sVector.Z;
		}
		
		//Triple product of vectors. Calc how determinant of matrix(coordinates of three vectors)
		public static double TripleProduct(Vector fVector, Vector sVector, Vector tVector)
		{
			return fVector.X* sVector.Y * tVector.Z +
				   fVector.Y * sVector.Z * tVector.X +
				   fVector.Z * sVector.X * tVector.Y -
				   fVector.Z * sVector.Y * tVector.X -
				   fVector.Y * sVector.X * tVector.Z -
				   fVector.X * sVector.Z * tVector.Y;
		}
		
        public override string ToString()
        {
            return String.Format("[{0}; {1}; {2}]", this.X, this.Y, this.Z);
        }
		
		//Calculate length vector
		public double length()
		{
			return Math.Sqrt(this.X * this.X + this.Y * this.Y + this.Z * this.Z);
		}
		
		//Calculates the angle between two vectors
		public static double AngleBetween(Vector fVector, Vector sVector)
		{
			return (Vector.scalar(fVector, sVector))/(fVector.length() * sVector.length());
		}
		
		public static bool operator < (Vector fVector, Vector sVector)
		{
			return (fVector.length() < sVector.length());
		}
		#endregion
		
		#region Operators
		public static bool operator > (Vector fVector, Vector sVector)
		{
			return (fVector.length() > sVector.length());
		}
		#endregion
	}
}